This is a repository where I will add my homework assignments!

Ex.3 LU#8

In this learning unit I learned what version control is and what are the major products available. I learned how important it is for a software development 
team. I learned the basics around GitLab, basics about Markdown. I learned the'if not x' command: it executes something if x evaluates to False. The opposite
of the 'if x' that we learned.

The first trading strategy consists of buying 10 units of Apple stock everyday for the duration of the period. This is naive since, by definition, it
does not take into account new information about the market and the company that is surely released during the period. In the end the bot spends $13055.4
($10000 were the capital based, the rest is borrowed). The portfolio contains 220 stocks valued at $109.99, for a total of $24197.80 . But the measure
of success we ought to look at is portfolio_value. This is equal to ending_cash + ending_value = $11142.40 . It is basically the final profit/loss of
the investment. We can then calculate the return of this strategy: 11.4% . Despite decent returns the strategy is still naive, it was somewhat lucky that
Apple stock kept increasing for the duration of the period.

The second trading strategy makes a one-time purchase of 100 units of Apple stock, and another 100 of Microsoft, and holds them indefinitely. At first
glance it might seems as naive as the previous strategy, however, I disagree. This strategy is passive investment. Which is something that legendary
investors such as Warren Buffet recommend. Research also points to superior returns by passive investment relative to active investment. In our simulation,
again we begin with $10000 worth of capital. The bot spends $15223. In the end, portfolio_value is $40529. Return is 305.29%. Although it should be noted
that the first simulation runs for 13 months. The second 5 years. Passive investment benefits grealy from time.