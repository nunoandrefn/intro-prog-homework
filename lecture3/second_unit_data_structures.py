#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 19:22:04 2018

@author: nunonobrega
"""

#Ex.0
#Index 2 0 1
my_tuple = ('a', 'b', 'c')
my_tuple[0]
my_tuple[1]
my_tuple[2]
print(my_tuple[0], my_tuple[1])

#Ex.1 tuple types dont support item assignment.

#Ex.2
apple_tuple = (100, 110, 120, 130, 140)
apple_tuple[0:2]

#Ex.3
#A tuple is a fixed object, it has no sort functionality because you cannot change its content 
#after creation.

#Ex.4
my_list = [100, 110, 120, 130, 140]
my_list.append(150)
my_list[5]

#Ex.5
#The key difference is that a list is dynamic. It has other functionalities attached such as
#adding new entries, sorting them.

#Ex.6
(1, 2, 3) == (1, 2, 3) #true
(1, 2, 3) == (2, 1, 3) #false
[1, 2, 3] == [1, 2, 3] #true
#When they are in the same order?

#Ex.7
#you use straight parens.

#Ex.8
#A dictionary data structure associates pairs of things: a key and a value. When I use the keys() function
#it returns a list of the keys which belong to the dictionary. When values() function it returns the values.
#When we use items() it returns the pairs.

#Ex.9
stock_prices = {}
stock_prices['hello'] = 'world'
stock_prices['SPX'] = 9000

#Ex.10
stock_prices = {
        'AAPL': 100,
        'GOOG': 99,
        }

stock_of_interest = 'AAPL'
print(stock_prices[stock_of_interest])
#It searches in the dictionary for the key assigned to stock_of_interest and prints its value.

#Ex.11
another_tuple = 1, 2, 3 #without parens?

L = list[1, 2, 3] #doesn't work

people = [Person("Nick", "Programmer"), Person("Alice","Engineer")]
professions = {}
for p in people:
    professions[p.name] = p.profession
    
#Ex.12
a_dic = {
    'SPX': 100,
    'TSLA': 120,
    'AAPL': 150,
    }
a_dic.items()

#Remove via .pop
a_dic.pop('AAPL')
#Remove via del
del a_dic['TSLA']

#.pop returns the value of a deleted key. del does not.

#Ex.13 create a function that takes only 1 argument which is a list.


def list_funct(list = ['wat', 'the']):
    list.append('homie')
    return list

list_funct()

#Ex.14
stock_dict = {'GOOG': 1126.79,
           'AAPL': 175.5,
           'KPMG': 89.1,
           'BCG': 95.3,
           'MSFT': 94.06,
           'OSYS': 39.9,
           'PHZR': 74.13,
           }
def ag_stock(stock_dict = {'GOOG': 1126.79, 'AAPL': 175.5, 'KPMG': 89.1, 'BCG': 95.3, 'MSFT': 94.06, 'OSYS': 39.9, 'PHZR': 74.13}):

#7 variables/arguments
def avg_stock(GOOG = 1126.79, AAPL = 175.5, KPMG=89.1, BCG=95.3, MSFT=94.06, OSYS=39.9, PHZR=74.13):
    ret_average = sum([GOOG, AAPL, KPMG, BCG, MSFT, OSYS, PHZR])/7
    return ret_average

#1 argument
stock_list = [1126.79, 175.5, 89.1, 95.3, 94.06, 39.9, 74.13]
print(sum(stock_list)/len(stock_list))
    

