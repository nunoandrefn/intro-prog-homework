#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 18:00:17 2018

@author: nunonobrega
"""

#Lecture 3
#Keyword arguments
def print_trade(ticker_symbol='AAPL', num_stocks=5):
    print('ricardo wants to trade', num_stocks, 'of', ticker_symbol) 
    
print_trade(num_stocks=10, ticker_symbol='SBE')

#Scope: if you dont learn the difference between global and local scope you may get code that runs wrongly
#but looks like it's doing the right thing. This is why understanding scope is important.

#Exercise 1
#Scope - the space where a variable is visible and can be used
#Local scope - the restricted space for variables initialized inside a function, they are only visible there
#Global scope - refers to the whole space of the text editor

#Ex.2
#first_var is in global scope. variable who is in a local scope and belongs to hello_world scope
#second_var is in global scope.
#variable who_again belongs to hello_other_world local scope

#Ex.3
print('hello', first_var)
first_var = 10
#There is an error. The order is the problem here, we must define the variable before trying to print it.

#Ex.4
a = 10
def simple_function():
    ret_value = a*2
    return ret_value

simple_function()
b = simple_function()
print(a, b)

#Ex.5
power = 10
print(power)

def generate_power(number):
    
    def nth_power():
        return number**power
    
    powered = nth_power()
    return powered


a = generate_power(2)
print(a)

#There is an error. We are trying to print _power_ but it is outside the scope.
#I expected it to work properly. Because all variables are within scope of the functions they should be.

#Ex.6
power = 10
print(power)

def generate_power(number):
    
    power = 2
    
    def nth_power():
        return number**power
    
    powered = nth_power()
    return powered

print(power)
a = generate_power(2)
print(a)
#No. Both prints will print the variable power defined in the global scope. When power is defined as 2, it
#is defined in a local scope. The print funct prioritizes variables in the same scope as it is?
#However, when I define the variable a and then print it I get 2^2=4. Because I used the func generate_power
#and within the local scope of this function power is defined as 2, so that definition is used.

#Ex.7
#I expect to see 'coins' and 'chocolate' printed. You shouldn't see the same thing for both prints. The print
#within the function should printed the variables defined in that function's local scope.
_what = 'coins'
_for = 'chocolate'

def transaction(_what, _for):
    _what = 'oranges'
    _for = 'bananas'
    print(_what, _for)
    
transaction(_what, _for)
print(_what, _for)
#In fact, we see both printed.

#Ex.8
#I don't think my perpective on these concepts changed.

#Ex.9
#Calling the function my_name with no argument
def my_name(first_name, last_name):
    first_name = 'nuno'
    last_name = 'nobrega'
    print(first_name, last_name)
my_name()

#With keyword argument
def my_name(first_name = 'nuno', last_name = 'nobrega'):
    print(first_name, last_name)
my_name(first_name, last_name)

#with two arguments
def my_name(first_name, last_name):
    print(first_name, last_name)
my_name('nuno', 'nobrega')

#Ex.10
first_name = 'Ricardo'
last_name = 'Pereira'

def name(first_name = 'Sam', last_name = 'Hopkins'):
    print(first_name, last_name)
    
name()
name(first_name = first_name, last_name = last_name)
name(first_name = 'Jose', last_name = 'Maria')

#Ex.11

def my_function(message = 'Good morning', name = ''):
    print(message, name)

my_function()
my_function(message = 'You know nothing', name = 'Jon Snow')