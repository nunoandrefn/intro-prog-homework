# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
print('hello world')
message = 'hello world'
print (message)

type('hello')
type(type)
type(print)

first_name = 'nuno'
last_name = 'nóbrega'
age = '22'
masters = 'economics'
bdate = '16/05/1995'

print(first_name)
print(age)
print(masters,bdate)

#Lecture 2 lu1 - variables

#Ex 1
a_string='TSLA'
a_float=200.5
an_integer=10
an_action='buy'
print('I would like to', an_action, an_integer, 'stocks in', a_string)

#Ex 2
player1='Bobby'
var_borrow='10'
player2='T-bone'
var_trade='50'
firm_name='SpaceX'
print(player1, 'needs to borrow', var_borrow, 'euros from', player2, 'in order to trade',
      var_trade, 'stocks in', firm_name)

#Ex 3
int_1 = 6
int_2 = 2
int_1*int_2
result=(int_1*int_2)

#Ex 4
celsius=30
fahrenheit = celsius*1.8+32
print(fahrenheit)
kelvin=celsius+273.15
print(kelvin)

#Slides exercise
def divide_by_two(n):
    ret_val = n/2
    return ret_val

a = 2
b = divide_by_two(a)
print('The value of a/2 is', b)

#Functions ex.1 lu2 - functions
def power(a,b):
    ret_value = a**b
    return ret_value
power(2, 2)
print(power(2, 2))

#ex.2
def truth_test (x, y):
    ret_value = a == b
    return ret_value
truth_test(6, 6)

#ex.3
def hypotenuse (a, b):
    ret_value = (a**(2)+b**(2))**0.5
    return ret_value
hypotenuse (2, 2)

#ex.4
def ltc (a):
    ret_value = a * 54.31
    return ret_value
ltc(2)

def eth (a):
    ret_value = a * 10.217524
    return ret_value
eth(2)

def euro (a):
    ret_value = a * 7023.24
    return ret_value
euro (1)

#ex.5

def euro (a):
    ret_value = a * 7023.24 #if I execute this there is no output, the function is defined by I don't tell
                            #it to do anything
    
print(euro(1)) #I figured this should yield the output, but it yields 'None'
type(euro) #It tells me it is a function.
euro(1) #Also yields 'None'. It seems that w/ return absent you cannot possibly get function output.

#ex.6

def truth_test (x, y):
    ret_value = a == b
    print(ret_value)
print(truth_test(6, 6))
#The return command executes the function, the print command lists the result of an executed function
#evaluated at some set of parameters.

#ex.7

#If what you type into the REPL returns null there is no Out. If you type anything but null it shows Out.
#The return value of the print function is also None. It prints whats inside it because thats what it does
#but it returns null.