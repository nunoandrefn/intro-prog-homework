#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 27 19:14:12 2018

@author: nunonobrega
"""

#Avoid putting underscores in your modules names. 

#Ex.1

import cat
speak()

from feline.cat import speak
speak()

#Ex.2