#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 27 18:05:35 2018

@author: nunonobrega
"""

#Flow control

if True:
    print('hello world')
    
if False:
    print('goodbye world')
    
a = True

if a:
    print('hello', a)
    
b = False

if b:
    print('hello, b')
    
if 1:
    print('hello', 1)
if 0:
    print('hello', 0)
if 2:
    print('hello', 2)
if 'world':
    print('hello', 'world')
if '':
    print('hello', '')
    
bool(True)
bool(False)
bool(0)
bool('')

if True:
    print('hello world')
def return_true():
    return True
if return_true():
    print('hello world')
val = return_true()
if val:
    print('hello world')
    
a = True
b = False
if a or a:
    print('at least one thing is True')
if a or b:
    print('at least one thing is True')
if b or b:
    print('at least one thing is True')
if b or b or b or b or a:
    print('at least one thing is True')
    
a_list = [1, 2, 3]
for num in a_list:
    print(num)
    
#Ex.1
bool(1) #True
bool(1.1) #True
bool(0) #False
bool(0.00001) #True
bool('') #False
bool(None) #False

#Ex.2
def a_function(symb='TSLA'):
    if symb == 'APPL':
        return True
    else:
        return False
    
#Ex.3
def gender_fun(gender = 'male'):
    if gender == 'male':
        return 'You like pink'
    elif gender == 'female':
        return 'You hate pink'
    else:
        return 'You are indifferent'
    
#Ex.4
def gender(gender):
    if gender == 'male':
        print('You hate pink')
    
    elif gender == 'female':
        print('You like pink')
        
        print('This line shouldnt be printed')
# Because with two if statements the function just runs everything. If we use elif, you tell the program that
# you want it to do one thing (if) or the other (elif).
        
#Ex.5
def age_diff(mother_age = 54, father_age = 53):
    if mother_age == father_age:
        return mother_age + father_age
    else:
        return mother_age - father_age
    
#Ex.6
dict1 = {'key1': 25, 'key2': 69, 'key3': 'wassup'}
list1 = [1, 2, 3, 4]
def cool_funct(dict1, list1):
    if len(dict1) == len(list1):
        return 'awesome'
    elif len(dict1) != len(list1):
        return 'that is sad'

#Ex.7
stock_dict = {'AAPL': 150, 'GOOG': 200, 'FB': 160}

for key in stock_dict:
    print(key) #prints keys

for key in stock_dict:
    print(stock_dict[key]) #prints values

for key in stock_dict:
    print('the stock price of', key, 'is', stock_dict[key])

#Ex.8
a_tuple = (10, 20, 30)
a_list = [40, 50, 60]
a_dict = {'AAPL': 100, 'GOOG': 90, 'FB': 80}

for num in a_tuple:
    print(num)
    
for num in a_list:
    print(num)
    
for key in a_dict:
    print(a_dict[key])
    
#Ex.9
def multiply_lists(list1):
    result = [x * 2 for x in list1]
    return result

#Ex.10
def multiply_dict(dict):
    result = {a_dict[key] * 2 for key in a_dict}
    return result

#Bonus: i tried to create a new dictionary like so:
    
new_dict = multiply_dict(a_dict)
#this creates a set with only the multiplied values of the original dict.
#How to create a dict instead?

def dict_multip(a_dict):
    new_dict = { }   
    for key in a_dict:
        new_dict[key] = 2*a_dict[key]

    return new_dict
new_dict = dict_multip(a_dict)

#Ex.11
def final_funct(list, number):
    if len(list) > number:
        return True
    else:
        return False